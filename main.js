import Vue from 'vue'
import App from './App'

import operate from "common/operate.js" //全局js
Vue.prototype.$operate = operate

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	...App,
})
app.$mount()
