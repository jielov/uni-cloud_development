'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	//event为客户端上传的参数
	console.log('event : ', event)

	const collection = db.collection('h5_user')

	// 查询用户名是否注册
	let user = await collection.where({
		username: event.username
	}).get()

	if (user.affectedDocs == 0) {
		
		let redirect = await collection.add({
			username: event.username,
			password: event.password,
		})
		
		return {
			code: 200,
			msg: '注册成功',
			data: redirect
		}
		
	} else {
		return {
			code: 200,
			msg: '用户名已注册',
			data: user
		}
	}



};
