'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	const collection = db.collection('holle')
	//event为客户端上传的参数
	console.log('event : ', event)

	// 添加数据
	// let res = await collection.add({
	// 	name: event.name
	// })

	//添加一个对象数组
	// let res = await collection.add([{
	// 	name: '测试1'
	// }, {
	// 	name: '测试2',
	// 	tiem: Date.now() //获取当前时间
	// }])

	// 删除数据
	// let res = await collection.doc('604ae3b233ae930001f67840').remove()

	// 修改数据
	// let res = await collection.doc('604ae03e9e89280001740a76').update({
	// 	name: '现在修改  测试'
	// })

	// 数据更新
	// let res = await collection.doc('604ae03e9e89280001740a76').set({
	// 	name: '现在修改  测试',
	// 	tiem: Date.now(), //获取当前时间
	// 	key: '更新出现的'
	// })

	// 查找数据 寻找key为 更新出现的                     
	let res = await collection.where({
		key: '更新出现的'
	}).get()

	console.log(JSON.stringify(res));
	return {
		code: 200,
		msg: '查询成功',
		data: res
	}



	// 根据客户端上传的数据查询
	// let res = await collection.where({
	// 	name: event.name
	// }).get()
	//返回数据给客户端
	// return event
};
