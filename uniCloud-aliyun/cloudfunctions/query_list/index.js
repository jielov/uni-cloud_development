'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	//event为客户端上传的参数
	console.log('event : ', event)

	const collection = db.collection('code_list')
	// 查询
	let query = await collection.where({
		testId: event.testId,
	}).get()

	//返回数据给客户端
	return {
		code: 200,
		msg: '查询成功',
		data: query
	}
};
