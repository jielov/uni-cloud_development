'use strict';

const db = uniCloud.database()
exports.main = async (event, context) => {

	const collection = db.collection('h5_user')
	// 账号密码登录
	let user = await collection.where({
		username: event.username,
		password: event.password,
	}).get()

	if (user.affectedDocs < 1) {
		return {
			code: 0,
			msg: '用户名或密码错误'
		}
	} else {
		return {
			code: 200,
			msg: '登录成功'
		}
	}
	//返回数据给客户端
	// return user
};
